//
//  ContentView.swift
//  Trackologist
//
//  Created by Cihan Hasanoglu on 05.12.19.
//  Copyright © 2019 Cihan Hasanoglu. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
